# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_LO.py $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/sqz/l1/guardian/SQZ_LO.py $

import time
from guardian import GuardState, GuardStateDecorator
from cdsutils import Servo
import numpy as np

from guardian.state import (
    TimerManager,
)
from noWaitServo import Servo


from ezca_automon import (
    EzcaEpochAutomon,
    EzcaUser,
    deco_autocommit,
    SharedState,
    DecoShared,
)

# SQZ config file (sqz/<ifo>/guardian/sqzconfig.py):
import sqzconfig

# this is to keep emacs python-mode happy from lint errors due
# to the namespace injection that Guardian does
if False:
    IFO = None
    ezca = None
    notify = None
    log = None

if sqzconfig.nosqz == True:
    nominal = 'IDLE'
else:
    #nominal = 'LOCKED'
    nominal = 'FINALIZED'

#nominal = 'LOCKED'
#nominal = 'IDLE'


def down_state_set():
    ezca['SQZ-LO_SERVO_COMBOOST'] = 0
    time.sleep(1)
    ezca['SQZ-LO_SERVO_SLOWBOOST'] = 0
    time.sleep(1)    
    ezca['SQZ-LO_SERVO_COMEXCEN'] = 0
    ezca['SQZ-LO_SERVO_FASTEXCEN'] = 0

    ezca['SQZ-LO_SERVO_IN1EN'] = False
    ezca['SQZ-LO_SERVO_IN2EN'] = False

    ezca['SQZ-LO_SERVO_IN1GAIN'] = -18 #-6
    ezca['SQZ-LO_SERVO_IN2GAIN'] = -20
    
    log('1')
    ezca.switch('SQZ-LO_SERVO_DC','INPUT','OFF')
    log('2')
    ezca.switch('SQZ-LO_SERVO_DC','FM10','OFF')
    log('3')    
    ezca.switch('SQZ-LO_SERVO_DC','FM10','ON')
    

class SQZLOParams(EzcaUser):
    HD_DC_THRESH   = 0.2   # FIX: make this a parameter

    # counter for lock attempts
    LOCK_ATTEMPT_COUNTER = 0
    LOCK_ATTEMPT_MAX = 5

    # these rails are for the CMB output and/or VCO input
    CMB_OUT_RAIL_HIGH = 4
    CMB_OUT_RAIL_LOW  = -4

    FAULT_TIMEOUT_S = 1
    UNLOCK_TIMEOUT_S = 1

    intervention_msg = None

    def __init__(self):
        self.timer = TimerManager(logfunc = None)
        self.timer['FAULT_TIMEOUT'] = 0
        self.timer['UNLOCK_TIMEOUT'] = 0


    def update_calculations(self):
        """
        No calculations, but keeping forwards compatible if they are wanted
        """
        return

    def fault_checker(self):
        fault_condition = False

        # beam diverter open?  OMC, else HD
        if ezca['SYS-MOTION_C_BDIV_E_OPENSWITCH'] == 1:
          # check green power and power on LO PD
          if ezca['OMC-DCPD_A_OUTMON'] < ezca['SQZ-LO_GRD_DC_MIN']:
            notify("Not enough power on OMC diode to lock")
            fault_condition = True
          if ezca['SQZ-OMC_TRANS_RF3_DEMOD_RFMON'] < ezca['SQZ-LO_GRD_RF_MIN']:
            notify("Not enough RF on OMC diode to lock")
            fault_condition = True
        else:
          # check green power and power on LO PD
          if ezca['SQZ-HD_A_DC_OUTMON'] < self.HD_DC_THRESH:
            notify("Not enough power on HD diode to lock")
            fault_condition = True
          if ezca['SQZ-HD_DIFF_RF3_DEMOD_RFMON'] < ezca['SQZ-LO_GRD_RF_MIN']:
            notify("Not enough RF on HD diode to lock")
            fault_condition = True

        if fault_condition:
            if self.timer['FAULT_TIMEOUT']:
                return 
            else:
                return True
        else:
            #reset the timeout every cycle
            self.timer['FAULT_TIMEOUT'] = self.FAULT_TIMEOUT_S
        return False

    def check_unlocked(self):
        unlock_condition = False

        # if the output is railed, we are not locked
        if ezca['SQZ-LO_SERVO_FASTMON'] < self.CMB_OUT_RAIL_LOW:
            log("CMB OUT RAILED LOW")
            unlock_condition = True
        if ezca['SQZ-LO_SERVO_FASTMON'] > self.CMB_OUT_RAIL_HIGH:
            log("CMB OUT RAILED HIGH")
            unlock_condition = True

        # if the SLOW output is railed, we are not locked
        if ezca['SQZ-LO_SERVO_SLOWMON'] < self.CMB_OUT_RAIL_LOW:
            log("CMB OUT RAILED LOW")
            unlock_condition = True
        if ezca['SQZ-LO_SERVO_SLOWMON'] > self.CMB_OUT_RAIL_HIGH:
            log("CMB OUT RAILED HIGH")
            unlock_condition = True


        # loops closed?
        #if not (ezca['SQZ-LO_SERVO_IN1EN'] or ezca['SQZ-LO_SERVO_IN2EN']):
            #unlocked_condition = True

        if unlock_condition:
            return True
        else:
            return False

class SQZLOShared(SharedState):
    @DecoShared
    def sqzlo(self):
        EzcaEpochAutomon.monkeypatch_subclass_instance(ezca)
        return SQZLOParams()

shared = SQZLOShared()


#############################################
#Functions

def beam_diverter_open():
    return ezca["SYS-MOTION_C_BDIV_E_OPENSWITCH"] == 1

def servo_set_for_omc():
    return ezca["SQZ-LO_SERVO_IN1EN"] == 1

def servo_set_for_hd():
    return ezca["SQZ-LO_SERVO_IN2EN"] == 1


#############################################
#Decorator

# There should be at least two types of fault_checker here.
# One you go back to trying again (lock loss), another you just go straight to down
# (no laser beam).

class lock_checker(GuardStateDecorator):
    def pre_exec(self):
        if shared.sqzlo.check_unlocked():
            if shared.sqzlo.timer['UNLOCK_TIMEOUT']:
                return 'LOCKLOSS'
        else:
            shared.sqzlo.timer['UNLOCK_TIMEOUT'] = shared.sqzlo.UNLOCK_TIMEOUT_S


class fault_checker(GuardStateDecorator):
    def pre_exec(self):
        if shared.sqzlo.fault_checker():
            return 'FAULT'

        else:
            shared.sqzlo.timer['FAULT_TIMEOUT'] = shared.sqzlo.FAULT_TIMEOUT_S

class servo_input_checker(GuardStateDecorator):
    def pre_exec(self):
        if servo_set_for_omc() and servo_set_for_hd():
            log("Both servo inputs are on, returning DOWN")
            return "DOWN"
        if beam_diverter_open() and servo_set_for_hd():
            log("Beam diverter is open but servo set for homodyne, returning DOWN")
            return "DOWN"
        if not beam_diverter_open() and servo_set_for_omc():
            log("Beam diverter is closed but servo is set for OMC, returning DOWN")
            return "DOWN"


class update_calculations(GuardStateDecorator):
    def pre_exec(self):
        shared.sqzlo.update_calculations()
        return


#############################################
#States

class INIT(GuardState):
    request = False

    def main(self):
        return True

    @deco_autocommit
    @update_calculations
    def run(self):
        return True


#exists only to inform the manager to move into its managed state
class MANAGED(GuardState):
    request = True

    @deco_autocommit
    @update_calculations
    def main(self):
        return True

    @deco_autocommit
    @update_calculations
    def run(self):
        return True


class IDLE(GuardState):
    index = 2
    request = True
    @deco_autocommit
    @update_calculations
    def run(self):
        return True


class LOCKLOSS(GuardState):
    """Record lockloss events"""
    request = False
    redirect = False

    def main(self):
        down_state_set()
        
        # count lock loss
        shared.sqzlo.LOCK_ATTEMPT_COUNTER += 1
        log("LOCK ATTEMPT " + str(shared.sqzlo.LOCK_ATTEMPT_COUNTER))

        if shared.sqzlo.LOCK_ATTEMPT_COUNTER > shared.sqzlo.LOCK_ATTEMPT_MAX:
            shared.sqzlo.LOCK_ATTEMPT_COUNTER = 0
            shared.sqzlo.intervention_msg = "Too Many Lock Failures"
            return 'REQUIRES_INTERVENTION'
        return True


class REQUIRES_INTERVENTION(GuardState):
    request = False
    redirect = False

    @update_calculations
    def run(self):
        if shared.sqzlo.intervention_msg is not None:
            notify(shared.sqzlo.intervention_msg)
        notify("request non-locking state")
        if ezca['GRD-SQZ_LO_REQUEST'] in lock_states:
            return False
        else:
            shared.sqzlo.intervention_msg = None
            return True


DOWN_RESET_TIME = 1
save_asc_gains = {}

    


# reset everything to the good values for acquistion.  These values
# are stored in the down script.
class DOWN(GuardState):
    index   = 1
    request = False
    goto    = True

    #@fault_checker
    @update_calculations
    def main(self):
        down_state_set()
        self.timer['down_settle'] = DOWN_RESET_TIME

    #@fault_checker
    @update_calculations
    def run(self):
        # only leave down state if ASC gains have ramped to zero
#        if self.timer['down_settle'] and all([
#                not ezca.is_ramping('SQZ-ASC_{}_{}_GAIN'.format(dof, ang)) for ang in ['P', 'Y'] for dof in ['POS', 'ANG']
#        ]):
            # reset ASC gains to nominal values
#            for dof in ['POS', 'ANG']:
#                for ang in ['P', 'Y']:
#                    if save_asc_gains.has_key((dof, ang)):
#                        ezca['SQZ-ASC_{}_{}_RSET'.format(dof, ang)] = 2
#                        ezca['SQZ-ASC_{}_{}_GAIN'.format(dof, ang)] = save_asc_gains[dof, ang]
            return True


class FAULT(GuardState):
    redirect = False
    request = False

    @update_calculations
    def main(self):
        down_state_set()

    @update_calculations
    def run(self):
        blocked = shared.sqzlo.fault_checker()
        if blocked is None:
            return
        if not blocked:
            return True


            
class LOCKING(GuardState):
    index = 5
    request = False
    
    #@fault_checker
    @update_calculations
    def main(self):
        self.counter = 0
        ezca['SQZ-LO_SERVO_IN1GAIN'] = -13
        #ezca['SQZ-LO_SERVO_IN1GAIN'] = max(-31,-16 - int(ezca['SQZ-CLF_REFL_RF6_DEMOD_RFMON']+28)/2) # gives UGF of 6.6kHz
        ezca['SQZ-LO_SERVO_IN2GAIN'] = max(-31,-18 - int(ezca['SQZ-CLF_REFL_RF6_DEMOD_RFMON']+28)/2) # gives UGF of 6.6kHz
        self.timer['done'] = 0
        log("LOCKING")
        return

    #@fault_checker
    @update_calculations
    def run(self):
        
        if not self.timer['done']:
            return

        if self.counter == 0:
            # beam diverter open?  OMC, else HD
            if ezca['SYS-MOTION_C_BDIV_E_OPENSWITCH'] == 1:
                # enable OMC input
                ezca['SQZ-LO_SERVO_IN1EN'] = 1
                self.timer['done'] = 1
                self.counter += 1
                
            else:
                notify('Open Beam diverter.')
                self.timer['done'] = 3
                return

        elif self.counter == 1:
            #ezca['SQZ-LO_SERVO_IN1GAIN'] += 1
            #self.timer['done'] = 0.5
            #Set to -16 for CLF target of -40dB, -6 for -47dB
            if True:#ezca['SQZ-LO_SERVO_IN1GAIN'] >= -16: #-6:
                self.counter += 1
                self.timer['done'] = 1

        elif self.counter == 1:
            self.timer['done'] = 2
            ezca['SQZ-LO_SERVO_COMBOOST'] = 1
            self.counter += 1

        elif self.counter == 2:
            self.timer['done'] = 2
            ezca['SQZ-LO_SERVO_COMBOOST'] = 2
            self.counter += 1

            
        elif self.counter == 3:            
            ezca['SQZ-LO_SERVO_SLOWBOOST'] = 1
            self.timer['done'] = 2
            ezca['SQZ-LO_SERVO_IN1GAIN'] = -12
            time.sleep(0.5)
            ezca['SQZ-LO_SERVO_IN1GAIN'] = -11
            time.sleep(0.5)
            ezca['SQZ-LO_SERVO_IN1GAIN'] = -10
            self.counter += 1
            

        elif self.counter == 4:
          return True


class SERVO_ENGAGED(GuardState):
    index = 10
    request = True
    
    #@servo_input_checker
    @lock_checker
    @update_calculations
    def main(self):
        self.timer['lock_settle'] = 2


    #@servo_input_checker
    @lock_checker
    @update_calculations
    def run(self):
        return True


class LOCKING_HD(GuardState):
    index = 21
    request = False
    
    
    @update_calculations
    def main(self):
        self.counter = 0
        #ezca['SQZ-LO_SERVO_IN1GAIN'] = -10 + (ezca['SQZ-CLF_REFL_RF6_DEMOD_RFMON']+16)/2
        self.timer['done'] = 0
        

    
    @update_calculations
    def run(self):
        
        if not self.timer['done']:
            return

        if self.counter == 0:
            # beam diverter open?  OMC, else HD
            if ezca['SYS-MOTION_C_BDIV_E_OPENSWITCH'] == 1:
                notify('Beam diverter is open, close it to lock HD')
                return
            else:
                ezca['SQZ-LO_SERVO_IN2EN'] = 1
                self.timer['done'] = 1
                self.counter += 1

        elif self.counter == 1:
            ezca['SQZ-LO_SERVO_IN1GAIN'] += 1
            self.timer['done'] = 0.5
            if ezca['SQZ-LO_SERVO_IN1GAIN'] >= -17:
                self.counter += 1
                self.timer['done'] = 1

        elif self.counter == 1:
            self.timer['done'] = 2
            ezca['SQZ-LO_SERVO_COMBOOST'] = 1
            self.counter += 1
            
        elif self.counter == 2:            
            ezca['SQZ-LO_SERVO_SLOWBOOST'] = 1
            self.timer['done'] = 2
            self.counter += 1

        elif self.counter == 3:
            self.timer['done'] = 2
            ezca['SQZ-LO_SERVO_COMBOOST'] = 2
            self.counter += 1

        elif self.counter == 4:
          return True

class HD_LOCKED(GuardState):
    index = 22
    request = True
    
    #@servo_input_checker
    #@fault_checker
    @lock_checker
    @update_calculations
    def main(self):
        self.timer['lock_settle'] = 2


    #@servo_input_checker
    #@fault_checker
    @lock_checker
    @update_calculations
    def run(self):
        return True
    
                
class FINALIZING(GuardState):
    index = 200
    request = False

    @lock_checker
    def main(self):
        self.timer['done'] = 0
        self.counter = 0

    @lock_checker
    def run(self):
        if not self.timer['done']:
            return

        if self.counter == 0:
            # engage DC
            ezca['SQZ-LO_SERVO_IN2GAIN'] = -20
            time.sleep(0.1)
            ezca['SQZ-LO_SERVO_IN2EN'] = 'On'
            time.sleep(0.1)
            
            self.timer['done'] = 2
            self.counter += 1

        elif self.counter == 1:
            ezca.switch('SQZ-LO_SERVO_DC','INPUT','ON')
            self.timer['done'] = 2
            self.counter += 1
            
        elif self.counter == 2:
            return True

class FINALIZED(GuardState):
    index = 300
    request = True

    @lock_checker
    def main(self):
        self.timer['lock_settle'] = 2


    @lock_checker
    def run(self):
        return True


class ENGAGING_SQZASC(GuardState):
    index = 500
    request = False

    

    def get_sensmatrix(filename):
        ZM4_OPTICALIGN = []
        ZM6_OPTICALIGN = []
        AS_A = []
        AS_B = []
        best_sqz = []
        min_sqz = []
        ang = []
        with open(filename,'r') as f:
            _lines = f.readlines()
            for line in _lines[1:]:
                s_line = line.split()
                if not len(s_line)==0:
                    ZM4_OPTICALIGN.append(float(s_line[0]))
                    ZM6_OPTICALIGN.append(float(s_line[1]))
                    AS_A.append(float(s_line[2]))
                    AS_B.append(float(s_line[3]))
                    best_sqz.append(float(s_line[4]))
                    min_sqz.append(float(s_line[5]))
                    ang.append(float(s_line[6]))


        AS_xy = np.array([AS_A,AS_B])        
        params_ASA, _ = curve_fit(sensing,xy,AS_A)
        params_ASB, _ = curve_fit(sensing,xy,AS_B)

        
        sens_mtrx = [[params_ASA[0],params_ASA[1]],[params_ASB[0],params_ASB[1]]]        
        return sens_mtrx

    def inmtrx(self):
        ZM4toASA = {}
        ZM4toASB = {}
        ZM6toASA = {}
        ZM6toASB = {}

        # DC current 50, CLF -40dB (see alog 66865), not blend 
       # ZM4toASA['P'] = 2.06e-3
       # ZM4toASB['P'] = 1.48e-3
       #ZM6toASA['P'] = 0
       # ZM6toASB['P'] = -0.44e-3
        
       # ZM4toASA['Y'] = -0.77e-3
       # ZM4toASB['Y'] = 0.46e-3
       # ZM6toASA['Y'] = 0.86e-3
       # ZM6toASB['Y'] = 0


        #ZM4toASA['P'] = 2.7e-3 # 0.16/50 [cnts/urad]
        #ZM4toASB['P'] = -1.9e-3 # -0.18/50
        #ZM6toASA['P'] = -1.89e-3 # -0.17/50
        #ZM6toASB['P'] = 0.2e-3 # 0

        ZM4toASA['P'] = 0.16/50  #[cnts/urad]
        ZM4toASB['P'] = -0.18/50
        ZM6toASA['P'] = -0.17/50
        ZM6toASB['P'] =  0
                
        #ZM4toASA['Y'] = -0.98e-3 # -0.04/50
        #ZM4toASB['Y'] = -3.7e-3 # -0.20/50
        #ZM6toASA['Y'] = 0.92e-3 # 0.1/50
        #ZM6toASB['Y'] = 1.8e-3 # 0.05/50
        
        ZM4toASA['Y'] = -0.04/50
        ZM4toASB['Y'] = -0.20/50
        ZM6toASA['Y'] = 0.1/50
        ZM6toASB['Y'] = 0.05/50

        
        
        for dof in ['P','Y']:
            sensmtrx = np.matrix([[ZM4toASA[dof],ZM6toASA[dof]],[ZM4toASB[dof],ZM6toASB[dof]]])
            inmtrx = np.linalg.inv(sensmtrx)

            ezca['SQZ-ASC_INMATRIX_%s_1_1'%dof] = inmtrx[0,0]
            ezca['SQZ-ASC_INMATRIX_%s_1_2'%dof] = inmtrx[0,1]
            ezca['SQZ-ASC_INMATRIX_%s_2_1'%dof] = inmtrx[1,0]
            ezca['SQZ-ASC_INMATRIX_%s_2_2'%dof] = inmtrx[1,1]

        
        
    @lock_checker
    def main(self):
        self.timer['waiting'] = 3
        self.counter = 0

        for dof in ['P','Y']:
            for ii in range(2):
                for jj in range(11):
                    ezca['SQZ-ASC_INMATRIX_%s_%d_%d'%(dof,ii+1,jj+1)] = 0

        ezca['SQZ-ASC_INMATRIX_P_1_1'] = 1
        ezca['SQZ-ASC_INMATRIX_P_2_2'] = 1
        ezca['SQZ-ASC_INMATRIX_Y_1_1'] = 1
        ezca['SQZ-ASC_INMATRIX_Y_2_2'] = 1

        for dof in ['P','Y']:
            for dof2 in ['POS','ANG']:
                ezca.switch('SQZ-ASC_%s_%s'%(dof2,dof),'FM1','OFF','FM2','ON')
                #ezca.switch('SQZ-ASC_%s_%s'%(dof2,dof),'FM1','OFF','FM2','INPUT','OUTPUT','ON') #This ran away jcb 16062023

        self.inmtrx()
        
    @lock_checker
    def run(self):
        if not self.timer['waiting']:
            return

        if self.counter == 0:
            if ezca['SQZ-ASC_WFS_GAIN'] >= 0.01:
                ezca['SQZ-ASC_WFS_GAIN'] = 0.01
            #if ezca['SQZ-ASC_WFS_GAIN'] >= 0.00:
            #    ezca['SQZ-ASC_WFS_GAIN'] = 0.00
                self.counter += 1
                self.timer['waiting'] = 1
            else:                
                ezca['SQZ-ASC_WFS_GAIN'] += 0.01
                self.timer['waiting'] = 0.2
        
        elif self.counter == 1:
            return True

class ENGAGING_SQZ_ADS(GuardState):
    index = 501
    request = False
    
    @lock_checker
    def main(self):
        self.timer['waiting'] = 3
        self.counter = 0

        for ii in range(4):
            ezca['SQZ-ASC_LO_INMTRX_1_%d'%(ii+1)] = 0
        ezca['SQZ-ASC_LO_INMTRX_1_2'] = 1
        
        for dof1 in ['PIT','YAW']:
            for ii in range(2):
                ezca['SQZ-ASC_ADS_%s%d_DEMOD_PHASE'%(dof1,ii+1)] = 0
                
        for dof in ['P','Y']:
            for ii in range(2):
                for jj in range(11):
                    ezca['SQZ-ASC_INMATRIX_%s_%d_%d'%(dof,ii+1,jj+1)] = 0

        ezca['SQZ-ASC_INMATRIX_P_1_8'] = 1
        ezca['SQZ-ASC_INMATRIX_P_2_9'] = 1
        ezca['SQZ-ASC_INMATRIX_Y_1_8'] = 1
        ezca['SQZ-ASC_INMATRIX_Y_2_9'] = 1

        for dof in ['P','Y']:
            for dof2 in ['POS','ANG']:
                ezca.switch('SQZ-ASC_%s_%s'%(dof2,dof),'FM1','ON','FM2','OFF')
                

    @lock_checker
    def run(self):
        if not self.timer['waiting']:
            return

        if self.counter == 0:
            # engage dither
            gainlist = {'PIT1':0,'PIT2':1000,'YAW1':0,'YAW2':2500}
            for dof in ['PIT1','PIT2','YAW1','YAW2']:
                ezca['SQZ-ASC_ADS_%s_OSC_TRAMP'%dof] = 3
                ezca['SQZ-ASC_ADS_%s_OSC_CLKGAIN'%dof] = gainlist[dof]
                ezca['SQZ-ASC_ADS_%s_DEMOD_I_GAIN'%dof] = 1                
            self.timer['waiting'] = 10
            self.counter += 1
            
        elif self.counter == 1:
            if ezca['SQZ-ASC_WFS_GAIN'] >= 0.2:
                ezca['SQZ-ASC_WFS_GAIN'] = 0.2
                self.counter += 1
                self.timer['waiting'] = 1
            else:                
                ezca['SQZ-ASC_WFS_GAIN'] += 0.02
                self.timer['waiting'] = 0.1
        
        elif self.counter == 2:
            return True
        
    
class LOCKED(GuardState):
    index = 1000
    request = True

    @lock_checker
    def main(self):
        self.timer['lock_settle'] = 2


    @lock_checker
    def run(self):
        return True




edges = [
    ('INIT', 'DOWN'),
    ('LOCKLOSS', 'DOWN'),
    ('FAULT', 'DOWN'),
    ('REQUIRES_INTERVENTION', 'DOWN'),    
    
    ('DOWN', 'IDLE'),
    ('IDLE', 'LOCKING'),    
    ('LOCKING', 'SERVO_ENGAGED'),
    ('SERVO_ENGAGED','FINALIZING'),
    ('FINALIZING','FINALIZED'),
    ('FINALIZED','ENGAGING_SQZASC'),
    #('ENGAGING_SQZASC','LOCKED'),
    ('FINALIZED','LOCKED'),
    
    #('LOCKED','ENGAGING_SQZ_ADS'),
    #('ENGAGING_SQZ_ADS','WAITING_SQZ_ADS'),
    #('WAITING_SQZ_ADS','ADS_ENGAGED'),
        
    ('IDLE','LOCKING_HD'),
    ('LOCKING_HD','HD_LOCKED'),
]


lock_states = [
    'LOCKING', 'LOCKED',
]



##################################################
'''
class LOCKED_LOPZT(GuardState):
    index = 20
    request = True
    
    #@servo_input_checker
    @lock_checker
    @update_calculations
    def main(self):
        self.timer['lock_settle'] = 2
        self.settled = False
        ezca['SQZ-LO_SERVO_COMBOOST'] = 2
        
        # create OPO temperature servo
        self.SEED_TEMP_servo = Servo(ezca, 'SQZ-LASER_HEAD_CRYSTALTEMPERATURE', readback='SQZ-LO_SERVO_SLOW_OUTPUT',
                                     gain=-1/20., ugf=0.15)
        log('tes')
        return

    #@servo_input_checker
    #@fault_checker
    @lock_checker
    @update_calculations
    def run(self):
        if self.timer['lock_settle']:
            log('tes')
            self.SEED_TEMP_servo.step()
            return True

class DISABLE_SQZ_ADS(GuardState):
    index = 70
    request = False
    
    @lock_checker
    def main(self):
        self.timer['waiting'] = 0
        self.counter = 0

    @lock_checker
    def run(self):
        if not self.timer['waiting']:
            return

        if self.counter == 0:
            ezca['SQZ-ASC_WFS_GAIN'] = 0
            self.counter += 1
            self.timer['waiting'] = 1
            
        elif self.counter == 1:            
            for dof in ['PIT1','PIT2','YAW1','YAW2']:
                ezca['SQZ-ASC_ADS_%s_OSC_CLKGAIN'%dof] = 0
                ezca['SQZ-ASC_ADS_%s_DEMOD_I_GAIN'%dof] = 1                
            self.timer['waiting'] = 2
            self.counter += 1
        
        elif self.counter == 2:
            return True

class ENGAGING_OMCQ_ADS(GuardState):
    index = 51
    request = False
    
    @lock_checker
    def main(self):
        self.timer['waiting'] = 3
        self.counter = 0

        

        # disable FC ADS
        #for dof1 in ['POS','ANG']:
        #    for dof2 in ['P','Y']:
        #        ezca.switch('SQZ-FC_ASC_INJ_%s_%s'%(dof1,dof2),'INPUT','OFF')
        for ii in range(4):
            ezca['SQZ-ASC_LO_INMTRX_1_%d'%(ii+1)] = 0
        ezca['SQZ-ASC_LO_INMTRX_1_1'] = 1
        
        for dof1 in ['PIT','YAW']:
            for ii in range(2):
                ezca['SQZ-ASC_ADS_%s%d_DEMOD_PHASE'%(dof1,ii+1)] = 180



                
                
    @lock_checker
    def run(self):
        if not self.timer['waiting']:
            return

        if self.counter == 0:
            # engage dither
            gainlist = {'PIT1':2000,'PIT2':2000,'YAW1':2000,'YAW2':2000}
            for dof in ['PIT1','PIT2','YAW1','YAW2']:
                ezca['SQZ-ASC_ADS_%s_OSC_TRAMP'%dof] = 3
                ezca['SQZ-ASC_ADS_%s_OSC_CLKGAIN'%dof] = gainlist[dof]
                ezca['SQZ-ASC_ADS_%s_DEMOD_I_GAIN'%dof] = 1                
            self.timer['waiting'] = 10
            self.counter += 1
            
        elif self.counter == 1:
            if ezca['SQZ-ASC_WFS_GAIN'] >= 0.025:
                ezca['SQZ-ASC_WFS_GAIN'] = 0.025
                self.counter += 1
                self.timer['waiting'] = 1
            else:                
                ezca['SQZ-ASC_WFS_GAIN'] += 0.005
                self.timer['waiting'] = 0.1
        
        elif self.counter == 2:
            return True
        
class WAITING_SQZ_ADS(GuardState):
    index = 55
    request = False

    DOFLIST = ['POS','ANG']
    
    def popappend(self,nplist,value):
        if nplist[-1] == value:
            return nplist
        else:
            return np.append(nplist[1:],value)

    @lock_checker
    def main(self):        
        self.counter = 0

        self.timer['done'] = 0
        self.checktime = 30


        self.dataT = 180 # sec
        self.timer['data_acq'] = self.dataT+1
        
        self.dataset = {}
        for dof1 in ['POS','ANG']:
            for dof2 in ['P','Y']:            
                self.dataset[dof1+dof2] = np.zeros(self.dataT*16)

    @lock_checker
    def run(self):
        if not self.timer['done']:
            return

        for dof1 in ['POS','ANG']:
            for dof2 in ['P','Y']:
                channame = 'SQZ-ASC_%s_%s'%(dof1,dof2)
                filt = ezca.get_LIGOFilter(channame)
                self.dataset[dof1+dof2] = self.popappend(self.dataset[dof1+dof2],ezca['%s_INMON'%(channame)]+ezca['%s_OFFSET'%(channame)]*filt.is_on('OFFSET'))

        if not self.timer['data_acq']:
            self.timer['ASC_zero'] = self.checktime
            notify('waiting for getdata')
            return

        ASC_done = True
        msg = 'waiting for '
        for dof1 in ['POS','ANG']:
            for dof2 in ['P','Y']:
                dataset = self.dataset[dof1+dof2]
                if abs(np.mean(dataset)) > np.std(dataset)/3:
                    ASC_done = False
                    msg += dof1 + ' ' + dof2 + ', '


        if ASC_done:
            return self.timer['ASC_zero']
        else:
            notify(msg)
            self.timer['ASC_zero'] = self.checktime
            return

class ADS_ENGAGED(GuardState):
    index = 100
    request = True
    
    #@servo_input_checker
    @lock_checker
    @update_calculations
    def main(self):
        self.timer['lock_settle'] = 2


    #@servo_input_checker
    @lock_checker
    @update_calculations
    def run(self):
        return True
        

'''
#######################################################################
